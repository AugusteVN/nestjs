import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Config } from './config';
import { TasksModule } from './tasks/tasks.module';
import { Task } from './tasks/tasks.entity';
import { AuthModule } from './auth/auth.module';
import { User } from './auth/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...Config.database,
      entities: [Task, User]
    }),
    TasksModule,
    AuthModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
