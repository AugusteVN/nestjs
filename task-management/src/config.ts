import { join } from 'path';
import { config } from 'dotenv-safe';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const envFiles = {
    test: join(__dirname, '../.env.local'),
    local: join(__dirname, '../.env.local'),
    sample: join(__dirname, '../.env.example')
}

// Get .env for current environment, default to local.
config({
    path: envFiles[process.env.NODE_ENV] || envFiles.local,
    example: envFiles.sample,
    allowEmptyValues: true
});

export class Config {
    static get auth() {
        return {
            jwtSecret: process.env.JWT_SECRET
        }
    }
    static get database(): TypeOrmModuleOptions {
        return {
            type: 'postgres',
            url: process.env.DATABASE_URL,
            entities: [join(__dirname, './../**/*.entity{.js,.ts}')],
            synchronize: true
        }
    }
}