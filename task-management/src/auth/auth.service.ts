import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRepo } from './user.repo';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth-creds.dto';
import { JwtService } from '@nestjs/jwt';
import { IJwtPayload } from './jwt-payload.interface';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserRepo) private _userRepo: UserRepo,
        private _jwtService: JwtService
    ) { }

    async signUp(authCredsDTO: AuthCredentialsDTO): Promise<void> {
        return this._userRepo.signUp(authCredsDTO);
    }

    async signIn(authCredsDTO: AuthCredentialsDTO): Promise<{ accessToken: string }> {
        const username = await this._userRepo.validatePassword(authCredsDTO);

        if (!username) {
            throw new UnauthorizedException('Invalid credentials');
        }

        const payload: IJwtPayload = { username };
        const accessToken = await this._jwtService.sign(payload);

        return { accessToken };
    }
}
