import { Controller, Post, Body, ValidationPipe, UseGuards, Req } from '@nestjs/common';
import { AuthCredentialsDTO } from './dto/auth-creds.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from './user.entity';
import { GetUser } from './get-user.decorator';

@Controller('auth')
export class AuthController {
    constructor(private _authService: AuthService) { }

    @Post('/signup')
    async signUp(@Body(ValidationPipe) authCredsDTO: AuthCredentialsDTO): Promise<void> {
        return this._authService.signUp(authCredsDTO);
    }

    @Post('/signin')
    async signIn(@Body(ValidationPipe) authCredsDTO: AuthCredentialsDTO): Promise<{ accessToken: string }> {
        return this._authService.signIn(authCredsDTO);
    }

    @Post('/test')
    @UseGuards(AuthGuard())
    test(@GetUser() user: User) {
        console.log(user);
    }
}
