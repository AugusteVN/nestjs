import { Repository, EntityRepository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDTO } from "./dto/auth-creds.dto";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepo extends Repository<User>{
    async signUp(authCredsDTO: AuthCredentialsDTO): Promise<void> {
        const { username, password } = authCredsDTO;

        const salt = await bcrypt.genSalt();

        const user = new User();
        user.username = username;
        user.password = await this.hashPassword(password, salt);
        user.salt = salt;

        try {
            await user.save();
        } catch (err) {
            if (err.code === '23505') {
                throw new ConflictException('Username already exists!');
            }
            throw new InternalServerErrorException();
        }
    }

    async validatePassword(authCredsDTO: AuthCredentialsDTO): Promise<string> {
        const { username, password } = authCredsDTO;
        const user = await this.findOne({ username });

        if (user && await user.validatePassword(password)) {
            return user.username;
        }
        return null;
    }

    private hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }
}