import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Config } from '../config';
import { IJwtPayload } from './jwt-payload.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepo } from './user.repo';
import { UnauthorizedException } from '@nestjs/common';

export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(@InjectRepository(UserRepo) private _userRepo: UserRepo) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: Config.auth.jwtSecret
        })
    }

    async validate(payload: IJwtPayload) {
        const { username } = payload;

        const user = this._userRepo.findOne({ username });

        if (!user) {
            throw new UnauthorizedException();
        }

        return user;
    }
}