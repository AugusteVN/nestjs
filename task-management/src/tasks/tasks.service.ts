import { Injectable } from '@nestjs/common';
import { TaskStatus } from './tasks.enum';
import { CreateTaskDTO } from './dto/create-task.dto';
import { TasksFilterDTO } from './dto/tasks-filter.dto';
import { TaskRepo } from './tasks.repo';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './tasks.entity';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';

@Injectable()
export class TasksService {
    constructor(@InjectRepository(TaskRepo) private _taskRepo: TaskRepo) {}

    async getTasks(
        filterDTO: TasksFilterDTO,
        @GetUser() user: User,
    ): Promise<Task[]> {
        return this._taskRepo.getTasks(filterDTO, user);
    }

    async getTaskById(id: number, @GetUser() user: User): Promise<Task> {
        return this._taskRepo.getTaskById(id, user);
    }

    async createTask(
        createTaskDTO: CreateTaskDTO,
        @GetUser() user: User,
    ): Promise<Task> {
        return this._taskRepo.createTask(createTaskDTO, user);
    }

    async updateTaskStatus(
        id: number,
        status: TaskStatus,
        @GetUser() user: User,
    ): Promise<Task> {
        return this._taskRepo.updateTaskStatus(id, status, user);
    }

    async deleteTask(id: number, @GetUser() user: User): Promise<void> {
        return this._taskRepo.deleteTask(id, user);
    }
}
