import { Task } from './tasks.entity';
import { Repository, EntityRepository } from 'typeorm';
import { CreateTaskDTO } from './dto/create-task.dto';
import { TaskStatus } from './tasks.enum';
import { NotFoundException } from '@nestjs/common';
import { TasksFilterDTO } from './dto/tasks-filter.dto';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';

@EntityRepository(Task)
export class TaskRepo extends Repository<Task> {
    async getTasks(
        filterDTO: TasksFilterDTO,
        @GetUser() user: User,
    ): Promise<Task[]> {
        const { status, search } = filterDTO || null;
        const query = this.createQueryBuilder('task');

        query.where('task.userId = :userId', { userId: user.id });

        if (status) {
            query.andWhere(`task.status = :status`, { status });
        }

        if (search) {
            query.andWhere(
                'task.title LIKE :search OR task.description LIKE :search',
                { search: `%${search}%` },
            ); // percentage sign makes it possible to search for partial strings or substrings.
        }

        const tasks = await query.getMany();

        return tasks;
    }

    async getTaskById(id: number, @GetUser() user: User): Promise<Task> {
        const found = await this.findOne({ where: { id, userId: user.id } });

        if (!found) {
            throw new NotFoundException(`Task with id ${id} not found.`);
        }

        return found;
    }

    async createTask(
        createTaskDTO: CreateTaskDTO,
        @GetUser() user: User,
    ): Promise<Task> {
        const { title, description } = createTaskDTO;

        const task: Task = new Task();
        task.title = title;
        task.description = description;
        task.status = TaskStatus.OPEN;
        task.user = user;

        await task.save();
        delete task.user; // GDPR
        return task;
    }

    async updateTaskStatus(
        id: number,
        status: TaskStatus,
        @GetUser() user: User,
    ): Promise<Task> {
        const task = await this.getTaskById(id, user);
        task.status = status;

        await task.save();
        return task;
    }

    async deleteTask(id: number, @GetUser() user: User): Promise<void> {
        const result = await this.delete({ id, userId: user.id });

        if (result.affected === 0) {
            throw new NotFoundException();
        }
    }
}
