import { PipeTransform, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { TaskStatus } from '../tasks.enum';

export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedStatuses = [
    TaskStatus.OPEN,
    TaskStatus.IN_PROGRESS,
    TaskStatus.DONE
  ];

  transform(value: string, metadata: ArgumentMetadata): string {
    value = value.toUpperCase();

    if (!this.isStatusValid(TaskStatus[value])) {
      throw new BadRequestException(`${value} is an invalid status`);
    }
    return value;
  }

  private isStatusValid(status: TaskStatus): boolean {
    const idx = this.allowedStatuses.indexOf(status);
    return idx !== -1;
  }

}